# import os
#
# env = os.getenv('Testing', 'DEV')
#
# class Config(object):
# 	SECRET_KEY = "thisissecretkeyforjwttoken"
# 	package_name = "com.widget.pos"
# 	class AWS:
# 		REGION_NAME = 'ap-south-1'
# 		AWS_ACCESS_KEY_ID = 'AKIA2YVUKFCK43DXOL43'
# 		AWS_SECRET_ACCESS_KEY = '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'
# 	class DynamoDB:
# 		if env == "DEV":
# 			TABLE_TRANSACTION_DETAILS = 'abfl_pos_transaction_details'
# 			TABLE_STATUS_CODE = 'abfl_pos_status_code'
# 			TABLE_VENDOR_DETAILS = 'vendor_details'
# 		else:
# 			TABLE_TRANSACTION_DETAILS = 'pos_transaction_details'
# 			TABLE_STATUS_CODE = 'pos_status_code'
# 			TABLE_VENDOR_DETAILS = 'pos_vendor_details'
#
# 	class SMS:
# 		sender_id = "ABLOAN"
# 		api_key = "Ade9047a16dea2f5e22924641d6851c27"
# 		url = "https://api-alerts.solutionsinfini.com/v4/?api_key="
# 		no_of_attempts_sms = 3
# 		no_of_attempts_verify = 3
# 		otp_validity_mins = 15
#
# 	class WRAPPER:
# 		if env=='DEV':
# 			username = 'dev_user'
# 			password = 'X2DHPcZjdNVC3gUUJQd56A=='
# 			base_url = "https://widget-dev-lms.abfldirect.com/"
# 		elif env == 'PROD':
# 			base_url = 'https://widget-lms.abfldirect.com/'
# 			username = 'prod_user'
# 			password = '53So5/cR9sHWcT9SCcfATg=='
# 		elif env == 'UAT':
# 			base_url = 'https://widget-lms-uat.abfldirect.com/'
# 			username = 'uat_user'
# 			password = 'Sy1mXMlbrcJ3cI1iVLPFXg=='
# 		version = '1.0'
# 		payment_status_api = '/paymentStatusAPI'
# 		payment_recipt_api = '/paymentReceiptAPI'
# 		get_request_amount_api = '/getRequestAmountAPI'
#
# 	class General:
# 		datetime_format = '%Y-%m-%d %H:%M:%S'

import os

env = os.getenv('Testing', 'DEV')
# env ="UAT"
class Config(object):
	SECRET_KEY = "thisissecretkeyforjwttoken"
	package_name = "com.widget.pos"

	class WRAPPER:
		payment_status_api = '/paymentStatusAPI'
		payment_recipt_api = '/paymentReceiptAPI'
		get_request_amount_api = '/getRequestAmountAPI'

	class General:
		datetime_format = '%Y-%m-%d %H:%M:%S'