import falcon
from app.core.utils.helpers import *
import json
from datetime import datetime as dt
import traceback
import ast
import base64
from app.core.utils.check_transaction import check_transaction

class CheckTransaction(object):
    def on_post(self, req, resp):
        log = []
        resp.context['log'] = log
        data = (req._params)
        print(data)
        try:
            missing_key_response = mandatoryFields(['mobile_number', 'bill_amount', 'transaction_id','order_id','version','partner_branch_id'], data)
            if missing_key_response:
                resp.body = json.dumps({'operation_status': '0', 'message': '', 'error_message': missing_key_response, 'status': 'GN_E_001', })
                resp.status = falcon.HTTP_400
            else:
                log.append("about to hit check transaction function")
                flag,lms_fail_flag,api_status_code,reason,status = check_transaction(data,log)
                log.append("flag is "+str(flag)+" lms fail flag is "+str(lms_fail_flag)+"api status is "+str(api_status_code)+" reason is "+str(reason)+" status is "+str(status))
                if flag:
                    resp.status = falcon.HTTP_200
                    resp.body = json.dumps({'operation_status': '1',
                                            'mobile_number': data['mobile_number'],
                                            "partner_branch_id" :data['partner_branch_id'],
                                            'order_id': data['order_id'],
                                            'transaction_id': data['transaction_id'],
                                            'bill_amount': int(data['bill_amount']),
                                            'status': status})
                else:
                    if lms_fail_flag:
                        resp.status = falcon.HTTP_500
                        resp.body = json.dumps({'operation_status': '0',
                                                'api': 'paymentReceiptStatus',
                                                'lms_response_code': api_status_code,
                                                'status': status,
                                                'reason': reason})
                    else:
                        resp.body = json.dumps(
                            {'operation_status': '0', 'message': '', 'status': status})
                        resp.status = falcon.HTTP_200
        except Exception as e:
            traceback.print_exc()
            log.append("error is "+str(e))
            resp.body = json.dumps({'operation_status': '0', 'message': 'Internal server Error', 'status': 'GN_E_005','reason':str(e)})
            resp.status = falcon.HTTP_500
        return
