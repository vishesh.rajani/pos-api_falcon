import falcon
from app.core.utils.helpers import *
import json
from datetime import datetime as dt
import ast
import base64
from app.core.utils.check_transaction import check_transaction

class ReloadRedis(object):
    def on_get(self, req, resp):
        try:
            load_configurations()
            resp.body = json.dumps({'Success': True})
            resp.status = falcon.HTTP_200
        except Exception as e:
            print (str(e))
            resp.body = json.dumps({'Success': False, 'reason': str(e)})
            resp.status = falcon.HTTP_500
