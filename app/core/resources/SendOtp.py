import falcon
from app.core.utils.helpers import *
from app.core.utils.send_otp import send_otp
import json
from datetime import datetime as dt
from datetime import datetime
import os
import traceback

class SendOtp(object):
    def on_post(self, req, resp):
        log = []
        resp.context['log'] = log
        data = req._params
        try:
            missing_key_response = mandatoryFields(['mobile_number', 'bill_amount', 'transaction_id', 'order_id','version','partner_branch_id'], data)
            if missing_key_response:
                resp.body = json.dumps({'operation_status': '0', 'message': '', 'error_message': missing_key_response,
                                        'status': 'GN_E_001', })
                resp.status = falcon.HTTP_400
            else:
                log.append("about to hit send_otp function")
                otp_sent,max_retry,status = send_otp(data)
                log.append("send otp flag is "+str(otp_sent)+" max retry is "+str(max_retry)+" status is "+str(status))
                resp.status = falcon.HTTP_200
                body = {"mobile_number": data['mobile_number'], "otp_sent": otp_sent,
                                            "max_retry": max_retry, "message": "", "status": status}
                if not max_retry and otp_sent:
                    body.update({"operation_status": "1"})
                else:
                    body.update({"operation_status": "0"})
                resp.body = json.dumps(body)
        except Exception as e:
            traceback.print_exc()
            log.append("error is "+str(e))
            resp.body = json.dumps({'operation_status': '0', 'message': 'Internal server Error', 'status': 'GN_E_005','reason':str(e)})
            resp.status = falcon.HTTP_500
        return
