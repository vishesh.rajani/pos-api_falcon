import falcon
import boto3
from app.aws_config import AWS
from app.core.utils.helpers import query_dynamo_db,get_redis_key_value,s3_obj
from app.redisfile import r
import json
import csv

class UploadStatusCodes(object):
    def on_get(self, req, resp):
        try:
            status_codes = s3_obj.get_object(Bucket=get_redis_key_value('STATUS_CODES','BUCKET_NAME'),Key=get_redis_key_value('STATUS_CODES','KEY'))
            status_codes = csv.reader(status_codes['Body'].read().decode('utf-8').splitlines(True), delimiter=',')
            print (status_codes)
            line_count = 0
            for row in status_codes:
                parameter = {
                    'status_code': row[0],
                    'status': row[1]
                }
                response = query_dynamo_db(get_redis_key_value('DYNAMODB','TABLE_STATUS_CODE'), 'INSERT', parameter=parameter)
                print (response['Success'])
                line_count = line_count + 1
            print ('Processed {line_count} lines'.format(line_count=line_count))
            resp.body = json.dumps({'Success':True})
            resp.status = falcon.HTTP_200
        except Exception as e:
            print (str(e))
            resp.body = json.dumps({'Success': False,'reason':str(e)})
            resp.status = falcon.HTTP_500
