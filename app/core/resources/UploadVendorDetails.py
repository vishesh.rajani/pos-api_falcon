import falcon
import boto3
from app.aws_config import AWS
import csv
from app.core.utils.helpers import query_dynamo_db,get_redis_key_value,s3_obj
from app.redisfile import r
import json

class UploadVendorDetails(object):
    def on_get(self, req, resp):
        try:
            vendor_details = s3_obj.get_object(Bucket=get_redis_key_value('VENDOR_DETAILS','BUCKET_NAME'),Key=get_redis_key_value('VENDOR_DETAILS','KEY'))
            vendor_details = csv.reader(vendor_details['Body'].read().decode('utf-8').splitlines(True), delimiter=',')
            for row in vendor_details:
                parameter = {
                    'id': int(row[0]),
                    'username': row[1],
                    'due_date': int(row[2]),
                    'name': row[3],
                    'password': row[4],
                    'scheme_list': row[5],

                }
                response = query_dynamo_db(get_redis_key_value('DYNAMODB','TABLE_VENDOR_DETAILS'), 'INSERT', parameter=parameter)
                print (response['Success'])
            resp.body = json.dumps({'Success':True})
            resp.status = falcon.HTTP_200
        except Exception as e:
            print (str(e))
            resp.body = json.dumps({'Success': False,'reason':str(e)})
            resp.status = falcon.HTTP_500
