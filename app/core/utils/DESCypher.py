import base64
from random import randint
from Crypto.Hash import MD5
from Crypto.Cipher import DES
import re
import base64
from Crypto import Random

def get_derived_key(password, salt, count):
	'''Function To Generate Dervied Key for Encryption/Decryption of Password'''
	password = password.encode('utf-8')
	salt = salt.encode('utf-8')
	hasher = MD5.new()
	hasher.update(password)
	hasher.update(salt)
	result = hasher.digest()
	for _ in range(1, count):
		hasher = MD5.new()
		hasher.update(result)
		result = hasher.digest()
	return result[:8], result[8:]


def decrypt_password(password,msg):
	'''Function To Decrypt The Password'''
	msg_bytes = base64.b64decode(msg)
	salt = '\xA9\x9B\xC8\x32\x56\x35\xE3\x03'
	enc_text = msg_bytes
	(dk, iv) = get_derived_key(password, salt, 2)
	crypter = DES.new(dk, DES.MODE_CBC, iv)
	text = crypter.decrypt(enc_text)
	return re.sub(r'[\x01-\x08]', '', text.decode('utf-8'))


def encrypt_password(password,msg):
	'''Function To Encrypt the Password'''
	salt = "\xA9\x9B\xC8\x32\x56\x35\xE3\x03"
	pad_num = 8 - (len(msg) % 8)
	for _ in range(pad_num):
		msg += chr(pad_num)
	(dk, iv) = get_derived_key(password, salt, 2)
	crypter = DES.new(dk, DES.MODE_CBC, iv)
	msg = msg.encode("utf-8")
	enc_text = crypter.encrypt(msg)
	return base64.b64encode(enc_text)

