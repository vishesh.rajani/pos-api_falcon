import falcon
from datetime import datetime as dt
from app.core.utils.helpers import query_dynamo_db,create_request_from_db, api_call
from app.core.config import Config
from app.core.utils.dynamodb_calls import save_transaction_details, get_transaction_details
from app.core.utils.wrapper_calls import call_payment_status

def check_transaction(data,log):
	status = None
	lms_fail_flag = False
	flag = False
	reason = None
	api_status_code = None
	response = get_transaction_details(data['transaction_id'],data['mobile_number'])
	if response['Success']:
		response = response['Row']
		data.update({'scheme_id':response['scheme_id']})
		if response['bill_amount'] == int(data['bill_amount']) and response['order_id'] == str(data['order_id']):
			if 'payment_receipt_ref_no' in response and len(response['payment_receipt_ref_no']) !=0:
				flag, api_status_code, reason, status = call_payment_status(data,response,log,True)
				log.append("status flag is "+str(flag)+"api status and reason is "+str(api_status_code)+str(reason)+"status is "+str(status))
				if not flag:
					lms_fail_flag =True
			else:
				status = 'CT_E_001'
				if 'reversed' in response and response['reversed']:
					status = 'CT_E_002'
		else:
			status='CT_E_002'
	else:
		status = 'CT_E_002'
	print(api_status_code,reason,status)
	return flag,lms_fail_flag,api_status_code,reason,status