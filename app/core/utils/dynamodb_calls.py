from app.core.utils.helpers import query_dynamo_db,get_redis_key_value
from app.core.config import Config

def get_transaction_details(transaction_id,mobile_number):
	condition = {'transaction_id': str(transaction_id), 'mobile_no': int(mobile_number)}
	response = query_dynamo_db(get_redis_key_value('DYNAMODB','TABLE_TRANSACTION_DETAILS'), 'QUERY', condition=condition)
	return response

def get_vendor_parameters(vendor_id):
	condition = {'id': int(vendor_id)}
	results = query_dynamo_db(get_redis_key_value('DYNAMODB','TABLE_VENDOR_DETAILS'), 'QUERY', condition=condition)
	return results

def get_vendor_parameters_by_username(username):
	results = query_dynamo_db(get_redis_key_value('DYNAMODB','TABLE_VENDOR_DETAILS'), 'QUERY-INDEX',
	                          condition={'username': str(username)})
	return results

def save_transaction_details(data):
	if 'mobile_number' in data:
		data.update({'mobile_no':int(data['mobile_number'])})
	query_dynamo_db(get_redis_key_value('DYNAMODB','TABLE_TRANSACTION_DETAILS'), 'INSERT', parameter=data)