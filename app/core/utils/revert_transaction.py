import falcon
from datetime import datetime as dt
from app.core.utils.helpers import query_dynamo_db
from app.core.config import Config
from app.core.utils.dynamodb_calls import save_transaction_details, get_transaction_details

def revert_transaction(data):
    status=None
    flag = False
    results = get_transaction_details(data['transaction_id'],data['mobile_number'])
    if results['Success']:
        status = 'RT_E_003'
    else:
        results = get_transaction_details(data['original_transaction_id'],data['mobile_number'])
        if results['Success']:
            results = results['Row']
            print(results)
            data.update({'scheme_id':results['scheme_id']})
            if data["bill_amount"] == str(results["bill_amount"]) and data['order_id'] == results['order_id']:
                if 'reversed' in results and results['reversed'] == True:
                    status = 'RT_E_004'
                else:
                    status = 'RT_S_001'
                    flag = True
                    date_issued = dt.now().day
                    date_revert = dt.strptime(results["timestamp"], Config.General.datetime_format).day
                    parameter = {'transaction_id': str(data['transaction_id']),
                                 'order_id': str(data['order_id']),
                                 'mobile_no': int(data['mobile_number']), 'vendor_name': str(data['vendor_name']),
                                 'scheme_id': str(data['scheme_id']),
                                 'original_transaction_id': str(data['original_transaction_id']),
                                 'bill_amount': int(data['bill_amount']), 'ref_type': 'R',
                                 'lan_no': str(results['lan_no']),
                                 'partner_branch_id':str(data['partner_branch_id'])}
                    if date_issued != date_revert:
                        flag = False
                        status = 'RT_E_002'
                        parameter.update({'status':'VOUCHER','reversed':True})
                    save_transaction_details(parameter)
            else:
                status='RT_E_001'
        else:
            status = 'RT_E_001'
    return flag,status
