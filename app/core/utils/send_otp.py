import json
from app.core.utils.helpers import generate_random_number_with_n_digits, query_dynamo_db, check_otp_validity,sms_otp,get_redis_key_value
import os
from app.core.utils.dynamodb_calls import get_transaction_details, save_transaction_details
from datetime import datetime as dt
import datetime
from app.core.config import Config

def send_otp(data):
    max_retry = False
    otp_sent = True
    status = None
    if os.getenv('Testing', 'DEV') == "PROD":
        otp = generate_random_number_with_N_digits(6)
    else:
        otp = 123456
    response = get_transaction_details(data['transaction_id'],data['mobile_number'])
    if response['Success'] and int(response['Row']['bill_amount']) == int(data['bill_amount']) and \
            response['Row']['order_id'] == data['order_id']:
        response = response['Row']
        data.update({'scheme_id':response['scheme_id']})
        if ('verified' in response and response['verified'] != 1) or (
                'otp' in response and 'verified' not in response):
            response['otp_timestamp'] = dt.strptime(response['otp_timestamp'],
                                                          Config.General.datetime_format)
            if not check_otp_validity(response['otp_timestamp']):
                response["otp"] = otp
                response["otp_timestamp"] = dt.now().strftime(Config.General.datetime_format)
                response["no_of_attempts_sms"] = 1
                response["no_of_attempts_verify"] = 0
            elif int(response["no_of_attempts_sms"]) >= int(get_redis_key_value('SMS','NO_OF_ATTEMPTS_SMS')):
                response['otp_timestamp'] = response['otp_timestamp'].strftime(
                    Config.General.datetime_format)
                max_retry = True
                otp_sent = False
            else:
                response['otp_timestamp'] = response['otp_timestamp'].strftime(
                    Config.General.datetime_format)
                response["no_of_attempts_sms"] = int(response["no_of_attempts_sms"]) + 1
                response["no_of_attempts_verify"] = 0
        else:
            response['otp'] = otp
            response['no_of_attempts_sms'] = 1
            response["no_of_attempts_verify"] = 0
            response['otp_timestamp'] = dt.now().strftime(Config.General.datetime_format)
    else:
        otp_sent=False
        status = "SO_E_002"
    if not max_retry and otp_sent:
        if os.getenv('Testing', 'DEV') == "PROD":
            sms_otp(mobile_number= data['mobile_number'],transaction_id= data['transaction_id'],bill_amount= data['bill_amount'],otp= response['otp'])
        response.update({'verified': 0})
        save_transaction_details(response)
        status = 'SO_S_001'
    elif max_retry:
        status = 'SO_E_001'
    return otp_sent,max_retry,status