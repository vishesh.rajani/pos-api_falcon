import falcon
from app.core.utils.helpers import *
import json
import base64
import os
import requests
from app.core.config import Config
from app.core.utils.dynamodb_calls import get_transaction_details, save_transaction_details, get_vendor_parameters
from datetime import datetime as dt

def call_get_request_amount(data,log):
	request_data = create_request("pos_get_request_amount", data)
	log.append("request data for get req amount is "+ str(request_data))
	flag=True
	status = None
	api_status_code= None
	response={}
	lms_wrapper_token = get_wrapper_token()
	log.append("wrapper token is "+str(lms_wrapper_token))
	headers = {'auth': lms_wrapper_token}
	r = api_call('get_request_amount',request_data,headers,data)
	log.append("get req amount response is "+ str(r))
	if r['status_code'] != 200:
		flag=False
		status = "GN_E_006"
		api_status_code = r['status_code']
	else:
		response = json.loads(r['content'])
		log.append("content is "+str(response))
		if response['servicableFlag'] == "YES":
			parameter = {'transaction_id': str(data['transaction_id']), 'order_id': str(data['order_id']),
			             'mobile_no': int(data['mobile_number']), 'vendor_name': str(data['vendor_name']),
			             'scheme_id': data['scheme_id'], 'bill_amount': int(data['bill_amount']),
			             'lan_no': str(response['loanNumber']),
			             'timestamp': str(dt.now().strftime(Config.General.datetime_format)), 'deal_no': response['dealNumber'],
			             'ref_type': 'D',
			             'partner_branch_id':str(data['partner_branch_id'])}
			save_transaction_details(parameter)
			status = 'VA_S_001'
		elif response['servicableFlag'] is None:
			operation_message = response['operationResponse']['operationMessage']
			operation_message = operation_message.replace(" ","")
			operation_message = operation_message.lower()
			if operation_message == "mobilenoisinvalid":
				status='VA_E_002'
			else:
				status = 'VA_E_001'
	return flag,api_status_code,status,response.get('customerName'),response.get('loanNumber')

def call_payment_recipt(data,response,log):
	payment_status_flag = False
	status = None
	api_status_code = None
	reason =None
	if 'ref_type' in response and response['ref_type'] == "D":
		results = get_vendor_parameters(data['vendor_id'])['Row']
		vendor_duedate = results['due_date']
		response['original_transaction_id'] = ""
		curr_date = dt.now()
		if str(curr_date.day) < str(vendor_duedate):
			duedate = get_month_and_year_due_date(1, vendor_duedate, curr_date.strftime("%Y-%m-%d"))
		else:
			duedate = get_month_and_year_due_date(2, vendor_duedate, curr_date.strftime("%Y-%m-%d"))
		response['due_date'] = duedate
		log.append("due date is " + str(duedate))
		request_data = create_request('pos_payment_recipt', response)
		response.pop('original_transaction_id')
	else:
		response['due_date'] = ""
		response['deal_no'] = ""
		request_data = create_request('pos_payment_recipt', response)
		response.pop('due_date')
		response.pop('deal_no')
	log.append("payment recipt request data is "+str(request_data))
	lms_wrapper_token = get_wrapper_token()
	headers = {'auth': lms_wrapper_token}
	r = api_call('payment_recipt',request_data,headers,data)
	log.append("payment recipt response is "+ str(r))
	api_status_code = r['status_code']
	if r['status_code'] == 200:
		content = json.loads(r['content'])
		log.append("payment recipt content is " + str(content))
		if content['operationStatus'] == "1":
			if content['paymentReceiptApiResponse'] is None:
				status = 'GN_E_006'
				reason = str(content['failureReason'])
			else:
				response['payment_receipt_ref_no'] = content['paymentReceiptApiResponse'][0]['paymentReceiptApiReferenceNo']
				result = save_transaction_details(response)
				print(result)
				payment_status_flag = True
		else:
			status = 'GN_E_006'
	else:
		status = 'GN_E_006'
	return payment_status_flag,api_status_code,reason,status

def call_payment_status(data,response,log,check_transaction_flag=None):
	flag =False
	status =None
	reason =None
	api_status_code = None
	request_data = create_request('pos_payment_status', response)
	log.append("payment status request data is "+str(request_data))
	lms_wrapper_token = get_wrapper_token()
	headers = {'auth': lms_wrapper_token}
	r = api_call('payment_status',request_data,headers,data)
	log.append("payment status response is "+str(r))
	api_status_code = r['status_code']
	if r['status_code'] == 200:
		content = json.loads(r['content'])
		log.append("content is "+str(content))
		print(content)
		if content['operationStatus'] != "1":
			status = 'GN_E_006'
		else:
			if content['crPmtReceiptRefResDtl'][0]['status'] == 'E':
				status = 'GN_E_006'
				reason = str(content['crPmtReceiptRefResDtl'][0]['process_ACTION'])
			else:
				if os.getenv('Testing', 'DEV') != "DEV" and check_transaction_flag is None:
					sms_otp(mobile_number= data['mobile_number'],transaction_id= data['transaction_id'],
					         bill_amount= response['bill_amount'],ref_type= response['ref_type'])
				response['status'] = True
				response['timestamp'] = str(dt.now().strftime(Config.General.datetime_format))
				if not check_transaction_flag:
					save_transaction_details(response)
				status = 'VO_S_001' if check_transaction_flag is None else 'CT_S_001'
				if response['ref_type'] == 'R':
					if check_transaction_flag:
						status = 'CT_S_002'
					else:
						status = 'VO_S_002'
						original_response = get_transaction_details(response['original_transaction_id'],data['mobile_number'])['Row']
						original_response['reversed'] = True
						original_response['reversed_timestamp'] = str(dt.now().strftime(Config.General.datetime_format))
						save_transaction_details(original_response)
				flag =True
	else:
		status='GN_E_006'
	return flag,api_status_code,reason,status