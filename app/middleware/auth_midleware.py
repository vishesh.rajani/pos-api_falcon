import json

import falcon
from app.core.config import Config
from app.core.utils.jwt import create_token
import base64
import calendar
from functools import wraps
from datetime import datetime as dt
import datetime
import jwt
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.fernet import Fernet
from app.core.utils.jwt import encrypt, decrypt


class AuthMiddleware(object):
	def process_request(self, req, resp):
		try:
			print(req.path.rsplit('/',1)[1])
			if req.path.rsplit('/',1)[1] in ['health','uploadStatusCodes','reloadRedis','uploadVendorDetails','token']:
				return
			if not req.headers.get('TOKEN'):
				resp.body = json.dumps({'operation_status': '0', 'status': 'missing token header', 'status_code': 'GN_E_002'})
				resp.status = falcon.HTTP_401
				resp.complete = True
				return
			try:
				token = req.headers.get('TOKEN')
				if token:
					vendor_details = jwt.decode(token, Config.SECRET_KEY, algorithms='HS256')
					print(vendor_details)
					vendor_id = decrypt(vendor_details['vendor_id'].encode())
					vendor_id = vendor_id.decode()
					print (vendor_id)
					vendor_name = decrypt(vendor_details['vendor_name'].encode())
					vendor_name = vendor_name.decode()
					req._params.update({'vendor_id':vendor_id,'vendor_name':vendor_name})
				else:
					resp.status = falcon.HTTP_401
					resp.body = json.dumps({'operation_status': '0', 'status': 'missing token header', 'status_code': 'GN_E_002'})
					resp.complete = True
					return
			except jwt.DecodeError:
				resp.body = json.dumps({'operation_status': '0', 'message': 'token is invalid', 'status': 'GN_E_003'})
				resp.status = falcon.HTTP_401
				resp.complete = True
				return
			except jwt.ExpiredSignature:
				resp.body = json.dumps({'operation_status': '0', 'message': 'token has expired', 'status': 'GN_E_004'})
				resp.status = falcon.HTTP_401
				resp.complete = True
				return
			except jwt.InvalidIssuedAtError:
				resp.body = json.dumps({'operation_status': '0', 'message': 'Issued at time doesnt look right', 'status': ''})
				resp.status = falcon.HTTP_401
				resp.complete = True
				return
			except jwt.InvalidIssuerError:
				resp.body = json.dumps({'operation_status': '0', 'message': 'Doesnt look like we issued this token',
				             'status': ''})
				resp.status = falcon.HTTP_401
				resp.complete = True
				return
			except Exception as e:
				print(e)
		except:
			pass
