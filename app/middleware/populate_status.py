import falcon
from app.core.utils.helpers import get_status
import json

class PopulateStatus(object):
	def process_response(self, req, resp, resource, req_succeeded):
		try:
			final_resp = json.loads(resp.body)
			final_resp = get_status(final_resp)
			resp.body = json.dumps(final_resp)
		except:
			pass