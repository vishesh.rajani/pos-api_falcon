from app.app import api
from app.core.resources.health import health
from app.core.resources.Token import Token
from app.core.resources.ValidateAmount import ValidateAmount
from app.core.resources.SendOtp import SendOtp
from app.core.resources.ValidateOtp import ValidateOtp
from app.core.resources.RevertTransaction import RevertTransaction
from app.core.resources.CheckTransaction import CheckTransaction
from app.core.resources.ReloadRedis import ReloadRedis
from app.core.resources.UploadStatusCodes import UploadStatusCodes
from app.core.resources.UploadVendorDetails import UploadVendorDetails

api.add_route('/pos/health', health())
api.add_route('/pos/token',Token())
api.add_route('/pos/validateAmount',ValidateAmount())
api.add_route('/pos/sendOtp',SendOtp())
api.add_route('/pos/validateOtp',ValidateOtp())
api.add_route('/pos/revertTransaction',RevertTransaction())
api.add_route('/pos/checkTransaction',CheckTransaction())
api.add_route('/pos/reloadRedis',ReloadRedis())
api.add_route('/pos/uploadStatusCodes',UploadStatusCodes())
api.add_route('/pos/uploadVendorDetails',UploadVendorDetails())